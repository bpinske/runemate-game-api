package com.runemate.ui.control;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.core.*;
import java.util.regex.*;
import javafx.application.*;
import javafx.embed.swing.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public class SlotControl extends StackPane {

    private final BotPlatform bot;
    private final Equipment.Slot slot;
    private @Getter Pattern pattern;

    private final ImageView item;

    public SlotControl(final BotPlatform bot, final Equipment.Slot slot) {
        this(bot, slot, null);
    }

    public SlotControl(final BotPlatform bot, final Equipment.Slot slot, final ItemDefinition initial) {
        this.bot = bot;
        this.slot = slot;

        setWidth(36);
        setMinWidth(36);
        setHeight(36);
        setMinHeight(36);
        setAlignment(Pos.CENTER);

        item = new ImageView();


        getStyleClass().add("slot-control");
        FXUtil.loadCss(this, "/css/slot_control.css");

        item.setFitWidth(32);
        item.setPreserveRatio(true);

        setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                refresh();
            }
            if (event.getButton() == MouseButton.SECONDARY) {
                clear();
            }
        });

        getChildren().addAll(item);

        if (initial != null) {
            set(initial, initial.stacks() ? 10 : 1);
        }
    }

    @SneakyThrows
    public void refresh() {
        log.debug("Refreshing item in {}", slot);
        final var equipped = bot.invokeAndWait(() -> Equipment.getItemIn(slot));
        ItemDefinition definition;
        if (equipped == null || (definition = bot.invokeAndWait(equipped::getDefinition)) == null) {
            clear();
            return;
        }

        set(definition, equipped.getQuantity());
    }

    public void clear() {
        item.setImage(null);
        pattern = null;
        Tooltip.install(this, new Tooltip("Empty"));
    }

    @SneakyThrows
    private void set(ItemDefinition definition, int quantity) {
        pattern = pattern(definition.getName());
        if (pattern == null) {
            clear();
            return;
        }

        Tooltip.install(this, new Tooltip(pattern.pattern()));
        final var item = new SpriteItem(definition.getId(), quantity);
        bot.invokeLater(() -> {
            final var image = item.getImage().get();
            if (image != null) {
                Platform.runLater(() -> this.item.setImage(SwingFXUtils.toFXImage(image, null)));
            }
        });
    }

    private static final Pattern SPECIAL = Pattern.compile("[{}()\\[\\].+*?^$\\\\|]");
    private static final Pattern CHARGED = Pattern.compile("\\\\\\(\\d+\\\\\\)");
    private static final Pattern BARROWS = Pattern.compile("^(Ahrim|Dharok|Guthan|Verac|Torag|Karil)'s ([a-zA-Z]+) ?(\\d+)?$");

    private Pattern pattern(String input) {
        log.trace("Input string '{}'", input);
        if (input == null || input.isEmpty()) {
            return null;
        }

        var barrows = BARROWS.matcher(input);
        if (barrows.matches()) {
            var brother = barrows.group(1);
            var piece = barrows.group(2);
            return Pattern.compile("^" + brother + "'s " + piece + " ?(\\d+)?$");
        }

        log.trace("Cleaned '{}'", escape(input));
        return Pattern.compile("^" + escape(input) + "$");
    }

    private String escape(String input) {
        input = SPECIAL.matcher(input).replaceAll("\\\\$0");
        input = CHARGED.matcher(input).replaceAll("\\\\((\\\\d+)\\\\)");
        return input;
    }

}

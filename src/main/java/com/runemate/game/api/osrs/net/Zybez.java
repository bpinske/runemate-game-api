package com.runemate.game.api.osrs.net;

import com.runemate.io.*;
import java.io.*;
import java.net.*;
import java.util.regex.*;
import lombok.extern.log4j.*;

/**
 * For use getting the prices of items in dead man mode.
 */
@Log4j2
public final class Zybez {
    private static final String API_URL_BASE =
        "http://forums.zybez.net/runescape-2007-prices/api/item/";
    private static final Pattern AVERAGE_PATTERN = Pattern.compile("average\":");

    private Zybez() {
    }

    public static int getAveragePrice(int itemId) {
        try (IOTunnel tunnel = new IOTunnel(new URL(API_URL_BASE + itemId).openStream())) {
            String page = new String(tunnel.readAsArray());
            if (page.contains("average\":")) {
                return (int) Double.parseDouble(AVERAGE_PATTERN.split(page)[1].split(",")[0]);
            } else {
                log.warn("Unable to get the price of {} because we didn't receive the expected response.", itemId);
            }
        } catch (IOException ioe) {
            log.warn("Unable to get the price of {}", itemId, ioe);
        }
        return -1;
    }
}
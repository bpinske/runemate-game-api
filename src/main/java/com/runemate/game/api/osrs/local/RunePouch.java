package com.runemate.game.api.osrs.local;

import static com.runemate.game.api.hybrid.local.VarbitID.*;

import com.runemate.game.api.hybrid.local.*;
import lombok.*;
import org.jetbrains.annotations.*;

public class RunePouch {

    private RunePouch() {
    }

    @Nullable
    public static Rune getRune(Slot slot) {
        return slot.getRune();
    }

    public static int getQuantity(Slot slot) {
        return slot.getQuantity();
    }

    public static int getQuantity(Rune rune) {
        for (final Slot slot : Slot.values()) {
            if (slot.getRune() == rune) {
                return slot.getQuantity();
            }
        }
        return 0;
    }

    public static boolean isEmpty() {
        return getEmptySlots() == 3;
    }

    public static int getEmptySlots() {
        int empty = 0;
        for (final Slot slot : Slot.values()) {
            if (slot.getQuantity() == 0) {
                empty++;
            }
        }
        return empty;
    }

    @AllArgsConstructor
    public enum Slot {
        ONE(RUNE_POUCH_RUNE1, RUNE_POUCH_AMOUNT1),
        TWO(RUNE_POUCH_RUNE2, RUNE_POUCH_AMOUNT2),
        THREE(RUNE_POUCH_RUNE3, RUNE_POUCH_AMOUNT3),
        FOUR(RUNE_POUCH_RUNE4, RUNE_POUCH_AMOUNT4);

        private final VarbitID type;
        private final VarbitID quantity;

        public Rune getRune() {
            final Varbit varbit = Varbits.load(type.getId());
            if (varbit != null) {
                final int value = varbit.getValue();
                for (final Rune rune : Rune.values()) {
                    if (rune.pouchType == value) {
                        return rune;
                    }
                }
            }
            return null;
        }

        public int getQuantity() {
            final Varbit varbit = Varbits.load(quantity.getId());
            return varbit == null ? 0 : varbit.getValue();
        }


        @Override
        public String toString() {
            return "RunePouch.Slot." + name();
        }
    }
}

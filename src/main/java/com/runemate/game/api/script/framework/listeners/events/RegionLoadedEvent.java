package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.location.*;
import org.jetbrains.annotations.*;
import lombok.*;

@Value
public class RegionLoadedEvent implements Event {

    Coordinate previousBase;
    Coordinate currentBase;
}

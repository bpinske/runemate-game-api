package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface GameObjectListener extends EventListener {

    default void onGameObjectSpawned(GameObjectSpawnEvent event) {
    }

}

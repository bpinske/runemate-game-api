package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.*;
import lombok.*;

@ToString
public class EngineStateEvent implements Event {

    private final RuneScape.EngineState previousState, currentState;

    public EngineStateEvent(int old, int current) {
        previousState = RuneScape.EngineState.of(old);
        currentState = RuneScape.EngineState.of(current);
    }

    public RuneScape.EngineState getPreviousState() {
        return previousState != null ? previousState : RuneScape.EngineState.UNKNOWN;
    }

    public RuneScape.EngineState getCurrentState() {
        return currentState != null ? currentState : RuneScape.EngineState.UNKNOWN;
    }
}

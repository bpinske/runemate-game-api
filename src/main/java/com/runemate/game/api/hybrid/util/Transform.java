package com.runemate.game.api.hybrid.util;

/**
 * A basic utility class that accepts an input, transforms it, and returns the same object as the output
 */
public interface Transform<T> {
    T transform(final T t);
}

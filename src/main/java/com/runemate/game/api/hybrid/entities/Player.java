package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.rs3.entities.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import lombok.*;
import javax.annotation.Nullable;

/**
 * A real-player's avatar
 */
public interface Player extends Actor {
    /**
     * Gets the adrenaline gauge that's available during combat
     *
     * @return The adrenaline gauge when in combat, otherwise null
     */
    @RS3Only
    @Nullable
    CombatGauge getAdrenalineGauge();

    /**
     * Gets the combat level of this player
     *
     * @return the combat level, otherwise -1
     */
    int getCombatLevel();

    /**
     * An RS3 exclusive that gets the currently summoned familiar.
     */
    @RS3Only
    @Nullable
    SummonedFamiliar getFamiliar();

    /**
     * @see Player#getNpcTransformationId()
     */
    @Deprecated
    default int getNpcId() {
        return getNpcTransformationId();
    }

    /**
     * The id of this players alternative, npc-based form.
     * Typically available when a player is transformed such as when a monkey at Ape Atoll.
     */
    int getNpcTransformationId();

    /**
     * @see #getTitleSuffix()
     */
    @RS3Only
    @Deprecated
    default String getTitlePostfix() {
        return getTitleSuffix();
    }

    /**
     * Gets this players title (when prefixed) (i.e. Lord, King)(RS3 only)
     */
    @RS3Only
    @Nullable
    @Deprecated
    String getTitlePrefix();

    /**
     * Gets this players title (when suffixed) (RS3 only)
     */
    @RS3Only
    @Nullable
    @Deprecated
    String getTitleSuffix();

    /**
     * The total skill level of this player.
     *
     * @return 0 if your weapon is unsheathed on RS3 and 0 if some unknown condition is fufilled on OSRS, otherwise the total level of the player
     */
    int getTotalLevel();

    /**
     * The level of the wilderness this player is in (RS3 only)
     */
    @RS3Only
    int getWildernessDepth();

    /**
     * Whether or not the player is female
     *
     * @return True if female, otherwise false
     */
    boolean isFemale();

    @Deprecated
    @NonNull
    List<ItemDefinition> getEquipment();

    @NonNull
    List<ItemDefinition> getWornItems();

    @Nullable
    ItemDefinition getWornItem(Equipment.Slot slot);

    int getTeamId();

    @OSRSOnly
    long getAppearanceHash();
}

package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import java.io.*;

public interface SerializableRequirement {
    int getOpcode();

    boolean serialize(ObjectOutput stream);

    boolean deserialize(int protocol, ObjectInput stream);
}

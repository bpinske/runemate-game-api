package com.runemate.game.api.hybrid.web.vertex.objects;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.hybrid.web.vertex.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@ToString
public abstract class ObjectVertex implements Vertex {

    protected final Coordinate position;
    protected final Pattern action;
    protected final @ToString.Exclude GameObjectQueryBuilder builder;

    public ObjectVertex(final Coordinate position, final Pattern action, final GameObjectQueryBuilder builder) {
        this.position = position;
        this.action = action;
        this.builder = builder;
    }

    @Nullable
    @Override
    public Coordinate getPosition() {
        return position;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ScanResult scan(final Map<String, Object> cache) {
        final var object = getObject();
        if (object == null) {
            return new ScanResult(null, ScanAction.CONTINUE);
        }

        final var previous = (Vertex) cache.get(WebPath.PREVIOUS);
        final var reachable = (Set<Coordinate>) cache.get(WebPath.REACHABLE);

        Coordinate stepBefore = null;
        if (previous != null) {
            Coordinate previousPos = previous.getPosition();
            if (reachable.contains(previousPos)) {
                stepBefore = previousPos;
            }
        }
        if (stepBefore == null) {
            final var area = object.getArea();
            if (area != null) {
                for (var surrounding : area.getSurroundingCoordinates()) {
                    if (reachable.contains(surrounding)) {
                        stepBefore = surrounding;
                    }
                }
            }
        }

        if (stepBefore == null || reachable.contains(stepBefore)) {
            if (object.isVisible()) {
                return new ScanResult(this, ScanAction.STOP);
            } else if (stepBefore != null && stepBefore.minimap().isVisible()) {
                return new ScanResult(new BasicVertex.Fake(position), ScanAction.STOP);
            }
        }

        return new ScanResult(null, ScanAction.CONTINUE);
    }

    @Nullable
    public GameObject getObject() {
        var results = builder.results();
        final var original = new ArrayList<>(results);
        if (results.size() > 1) {
            log.warn("Ambiguous search results for {}, confining results to {}", this, position);
            results = GameObjects.newQuery().provider(() -> original).on(position).results();
        }
        if (results.isEmpty()) {
            log.warn("Failed to find target entity on {}, expanding search", position);
            results = GameObjects.newQuery().provider(() -> original).within(new Area.Circular(position, 3)).results();
        }
        return results.nearest();
    }
}

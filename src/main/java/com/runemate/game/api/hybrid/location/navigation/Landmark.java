package com.runemate.game.api.hybrid.location.navigation;

import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities.*;

public enum Landmark {
    BANK(BankVertex.class),
    DEPOSIT_BOX(DepositBoxVertex.class),
    GRAND_EXCHANGE_CLERK(GrandExchangeVertex.class),
    MUSICIAN(MusicianVertex.class),
    PRAYER_ALTAR(PrayerAltarVertex.class);
    private final Class<? extends WebVertex> vertexType;

    Landmark(Class<? extends WebVertex> vertexType) {
        this.vertexType = vertexType;
    }

    public Class<? extends WebVertex> getVertexType() {
        return vertexType;
    }
}

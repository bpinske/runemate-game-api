package com.runemate.game.api.hybrid.structures;

import com.runemate.game.cache.io.*;
import java.io.*;

public class CoordinateSpace {
    private Quaternion quaternion;
    private Vector3f vector3f;
    private Vector3f otherVector3f;

    public CoordinateSpace(Js5InputStream stream, boolean parseCoordinateSpaceAllAtOnce)
        throws IOException {
        if (parseCoordinateSpaceAllAtOnce) {
            this.parseCoordinateSpace(stream);
        } else {
            this.quaternion = new Quaternion(stream);
            this.vector3f = new Vector3f(stream);
            this.otherVector3f = new Vector3f(stream);
        }
    }

    void parseCoordinateSpace(Js5InputStream stream) throws IOException {
        int mask = stream.readUnsignedByte();
        float f2 = 0.0f;
        float f3 = 0.0f;
        float f4 = 0.0f;
        float f5 = 1.0f;
        if ((mask & 1) != 0) {
            f2 = (float) stream.readShort() / 32768.0f;
            f3 = (float) stream.readShort() / 32768.0f;
            f4 = (float) stream.readShort() / 32768.0f;
            f5 = (float) stream.readShort() / 32768.0f;
        }
        //this.quaternion = new Quaternion(f2, f3, f4, f5);
        float f6 = 0.0f;
        float f7 = 0.0f;
        float f8 = 0.0f;
        if ((mask & 2) != 0) {
            f6 = stream.readShort();
        }
        if ((mask & 4) != 0) {
            f7 = stream.readShort();
        }
        if ((mask & 8) != 0) {
            f8 = stream.readShort();
        }
        //this.vector3f = new Vector3f(f6, f7, f8);
        float f9 = 1.0f;
        float f10 = 1.0f;
        float f11 = 1.0f;
        if ((mask & 0x10) != 0) {
            f10 = f11 = (float) stream.readShort() / 128.0f;
            f9 = f11;
        } else {
            if ((mask & 0x20) != 0) {
                f9 = (float) stream.readShort() / 128.0f;
            }
            if ((mask & 0x40) != 0) {
                f10 = (float) stream.readShort() / 128.0f;
            }
            if ((mask & 0x80) != 0) {
                f11 = (float) stream.readShort() / 128.0f;
            }
        }
        //this.otherVector3f = new Vector3f(f9, f10, f11);
    }
}

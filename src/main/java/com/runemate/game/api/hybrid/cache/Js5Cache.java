package com.runemate.game.api.hybrid.cache;

import com.runemate.game.cache.*;

public final class Js5Cache {

    private Js5Cache() {
    }

    /**
     * Loads a file from the game's Js5 cache. This performs an IO operation every call.
     */
    public static byte[] load(int archive, int group, int file) {
        return new DataLoader(archive).retrieve(JS5CacheController.getLargestJS5CacheController(), group, file);
    }

    /**
     * @deprecated RS3 support is removed
     */
    @Deprecated
    public interface RS3Archives {

        int JS5_ANIMS = 0;
        int JS5_BASES = 1;
        int JS5_CONFIG = 2;
        int JS5_INTERFACES = 3;
        int JS5_MAPS = 5;
        int JS5_MODELS = 7;
        int JS5_SPRITES = 8;
        int JS5_BINARY = 10;
        int JS5_CLIENTSCRIPTS = 12;
        int JS5_FONTMETRICS = 13;
        int JS5_VORBIS = 14;
        int JS5_CONFIG_LOC = 16;
        int JS5_CONFIG_ENUM = 17;
        int JS5_CONFIG_NPC = 18;
        int JS5_CONFIG_OBJ = 19;
        int JS5_CONFIG_SEQ = 20;
        int JS5_CONFIG_SPOT = 21;
        int JS5_CONFIG_STRUCT = 22;
        int JS5_WORLDMAPDATA = 23;
        int JS5_QUICKCHAT = 24;
        int JS5_QUICKCHAT_GLOBAL = 25;
        int JS5_MATERIALS = 26;
        int JS5_CONFIG_PARTICLE = 27;
        int JS5_DEFAULTS = 28;
        int JS5_CONFIG_BILLBOARD = 29;
        int JS5_DLLS = 30;
        int JS5_SHADERS = 31;
        int JS5_LOADING_SPRITES = 32;
        int JS5_LOADING_SCREENS = 33;
        int JS5_LOADING_SPRITES_RAW = 34;
        int JS5_CUTSCENES = 35;
        int JS5_AUDIOSTREAMS = 40;
        int JS5_WORLDMAPAREADATA = 41;
        int JS5_WORLDMAPLABELS = 42;
        int JS5_TEXTURES_DIFFUSE_PNG = 43;
        int JS5_TEXTURES_HDR_PNG = 44;
        int JS5_TEXTURES_DIFFUSE_DXT = 45;
        int JS5_TEXTURES_HDR_PNG_MIPPED = 46;
        int JS5_MODELSRT7 = 47;
        int JS5_ANIMSRT7 = 48;
        int JS5_DBTABLEINDEX = 49;
    }

    /**
     * @deprecated Replaced by {@link CacheIndex}
     */
    @Deprecated
    public interface OSRSArchives {

        int JS5_CONFIG = 2;
        int JS5_MAPS = 5;
        int JS5_MODELS = 7;
        int JS5_SPRITES = 8;
    }


}

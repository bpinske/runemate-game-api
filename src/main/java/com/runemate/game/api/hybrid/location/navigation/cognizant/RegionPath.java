package com.runemate.game.api.hybrid.location.navigation.cognizant;

import static com.runemate.game.api.hybrid.player_sense.PlayerSense.Key.*;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.basic.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.incubating.util.*;
import java.util.*;
import java.util.stream.*;
import org.jetbrains.annotations.*;

/**
 * A path generated using clipping data regarding the local player's current region.
 * Can generate perfect paths assuming the entire path exists in the current region
 * and there aren't any obstacles that the path would need to traverse.
 */
public final class RegionPath extends CoordinatePath {

    private final List<Coordinate> path;

    private RegionPath(List<Coordinate> steps) {
        this.path = steps;
    }

    /**
     * Generates a path from your local position to the first destination that it can reach.
     * Passing multiple destinations is most beneficial when you're not entirely sure which object or npc would be fastest to reach.
     * This implementation is significantly faster than several independent searches.
     */
    @Nullable
    public static RegionPath buildTo(final Locatable... destinations) {
        if (destinations == null || destinations.length == 0) {
            return null;
        }
        return buildTo(Arrays.asList(destinations));
    }

    /**
     * Generates a path from your local position to the first destination that it can reach.
     * Passing multiple destinations is most beneficial when you're not entirely sure which object or npc would be fastest to reach.
     * This implementation is significantly faster than several independent searches.
     */
    @Nullable
    public static RegionPath buildTo(final Collection<? extends Locatable> destinations) {
        if (destinations == null || destinations.isEmpty()) {
            return null;
        }
        return buildBetween(Players.getLocal(), destinations);
    }

    /**
     * Generates a path from start to the first destination that it can reach.
     * Passing multiple destinations is most beneficial when you're not entirely sure which object or npc would be fastest to reach.
     * This implementation is significantly faster than several independent searches.
     */
    @Nullable
    public static RegionPath buildBetween(
        final Locatable start, final Collection<? extends Locatable> destinations
    ) {
        return buildBetween(start, destinations, null);
    }

    /**
     * For caching, generates a path from start to the first destination that it can reach using the provided collision flags.
     * Passing multiple destinations is most beneficial when you're not entirely sure which object or npc would be fastest to reach.
     * This implementation is significantly faster than several independent searches.
     */
    @Nullable
    public static RegionPath buildBetween(
        final Locatable start, final Collection<? extends Locatable> destinations, int[][][] collision_flags, final Coordinate regionBase
    ) {
        int plane = -1;
        if (start != null) {
            Coordinate position = start.getPosition();
            if (position != null) {
                plane = position.getPlane();
            }
        }
        if (plane == -1) {
            return null;
        }
        return buildBetween(start, destinations, collision_flags != null ? collision_flags[plane] : null, regionBase);
    }

    @Nullable
    public static RegionPath buildBetween(
        final Locatable start, final Collection<? extends Locatable> destinations, int[][] flags2d, final Coordinate regionBase
    ) {
        if (start == null || destinations == null || regionBase == null || destinations.isEmpty()) {
            return null;
        }
        Coordinate spos = start.getPosition();
        if (spos == null) {
            return null;
        }
        List<Coordinate> dposs = new ArrayList<>(destinations.size());
        for (Locatable destination : destinations) {
            Coordinate dpos = destination.getPosition();
            if (dpos == null || dpos.getPlane() != spos.getPlane()) {
                continue;
            }
            dposs.add(dpos);
        }
        ((ArrayList<?>) dposs).trimToSize();
        if (dposs.isEmpty()) {
            return null;
        }
        List<Coordinate> steps;
        if (flags2d != null) {
            Area.Rectangular pathingRegion = new Area.Rectangular(regionBase, regionBase.derive(flags2d.length - 1, flags2d.length - 1));
            if (!pathingRegion.contains(spos)) {
                return null;
            }
            dposs = dposs.stream().filter(pathingRegion::contains).collect(Collectors.toList());
            if (dposs.isEmpty()) {
                return null;
            }
            steps = Dijkstra.findPath(spos, dposs, flags2d, regionBase);
        } else {
            Area.Rectangular pathingRegion =
                new Area.Rectangular(regionBase, regionBase.derive(Region.getWidth() - 1, Region.getHeight() - 1));
            if (!pathingRegion.contains(spos)) {
                return null;
            }
            dposs = dposs.stream().filter(pathingRegion::contains).collect(Collectors.toList());
            if (dposs.isEmpty()) {
                return null;
            }
            steps = Dijkstra.findPath(spos, dposs, Region.getCollisionFlags(spos.getPlane()), regionBase);
        }
        if (steps == null) {
            return null;
        }
        return new RegionPath(steps);
    }

    @Nullable
    public static RegionPath buildBetween(
        final Locatable start, final Collection<? extends Locatable> destinations, final int[][][] collision_flags
    ) {
        return buildBetween(start, destinations, collision_flags, Region.getBase());
    }

    /**
     * Gets the amount of milliseconds that it took to generate this path.
     */
    @Deprecated
    public long getGenerationTime() {
        return -1;
    }

    @Override
    public List<Coordinate> getVertices() {
        return path;
    }

    private static final class Dijkstra {

        /**
         * Finds a path with djikstra.
         */
        private static List<Coordinate> findPath(
            final Coordinate start,
            final Collection<Coordinate> destinations,
            final int[][] flags2d,
            final Coordinate region_base
        ) {
            if (flags2d == null) {
                return null;
            }
            final HashMap<Coordinate, Double> real_distances = new HashMap<>(100);
            final HashMap<Coordinate, Coordinate> traversed_tree = new HashMap<>(100);
            final ArrayList<Coordinate> queue = new ArrayList<>(100);
            real_distances.put(start, 0.0D);
            queue.add(start);
            while (!queue.isEmpty()) {
                Coordinate current = popBasedOnDistance(start, queue, real_distances);
                for (Coordinate destination : destinations) {
                    if (Objects.equals(current, destination)) {
                        LinkedList<Coordinate> path = new LinkedList<>();
                        path.addFirst(destination);
                        while (true) {
                            destination = traversed_tree.get(destination);
                            if (destination == null) {
                                break;
                            }
                            path.addFirst(destination);
                        }
                        return new ArrayList<>(path);
                    }
                }
                final double cost_to_current = real_distances.get(current);
                final List<Coordinate> successors = successors(current, region_base.getX(), region_base.getY(), flags2d);
                Collections.shuffle(successors, Random.getRandom());
                for (final Coordinate successor : successors) {
                    final double cost_to_successor_from_current = Distance.between(current, successor, Distance.Algorithm.MANHATTAN);
                    if (cost_to_successor_from_current == Double.POSITIVE_INFINITY) {
                        continue;
                    }
                    var xd = successor.getX() - region_base.getX();
                    var yd = successor.getY() - region_base.getY();
                    if (xd <= 0 || xd >= 104 || yd <= 0 || yd >= 104) {
                        continue;
                    }
                    double estimated_cost_to_successor = cost_to_current + cost_to_successor_from_current;
                    int successor_flag = flags2d[xd][yd];
                    int northernFlag = flags2d[xd][yd + 1];
                    int southernFlag = flags2d[xd][yd - 1];
                    int easternFlag = flags2d[xd + 1][yd];
                    int westernFlag = flags2d[xd - 1][yd];
                    if (CollisionUtils.hasBoundaryOnOneOrMoreSides(successor_flag, northernFlag, southernFlag, easternFlag, westernFlag)) {
                        //We have a wall nearby, let's avoid with a weight.
                        estimated_cost_to_successor += (cost_to_successor_from_current * PlayerSense.getAsDouble(WALL_AVOIDANCE_MODIFIER));
                    }
                    final Double previous_cost_to_successor = real_distances.get(successor);
                    if (previous_cost_to_successor == null || estimated_cost_to_successor < previous_cost_to_successor) {
                        real_distances.put(successor, estimated_cost_to_successor);
                        traversed_tree.put(successor, current);
                        queue.add(successor);
                    }
                }
            }
            return null;
        }

        private static List<Coordinate> successors(
            final Coordinate current,
            final int region_base_x,
            final int region_base_y,
            final int[][] planeFlags
        ) {
            int region_x = current.getX() - region_base_x;
            int region_y = current.getY() - region_base_y;
            if (region_x < 0 || region_x >= planeFlags.length || region_y < 0 || region_y >= planeFlags[region_x].length) {
                return Collections.emptyList();
            }
            final List<Coordinate> successors = new ArrayList<>(4);
            final int baseFlag = planeFlags[region_x][region_y];
            Coordinate cardinal = current.derive(0, 1);
            region_x = cardinal.getX() - region_base_x;
            region_y = cardinal.getY() - region_base_y;
            int cardinalFlag;
            if (region_x >= 0 && region_x < planeFlags.length && region_y >= 0 && region_y < planeFlags[region_x].length) {
                cardinalFlag = planeFlags[region_x][region_y];
                if (CollisionUtils.noNorthernBoundary(baseFlag, cardinalFlag)) {
                    successors.add(cardinal);
                }
            }
            cardinal = current.derive(0, -1);
            region_x = cardinal.getX() - region_base_x;
            region_y = cardinal.getY() - region_base_y;
            if (region_x >= 0 && region_x < planeFlags.length && region_y >= 0 && region_y < planeFlags[region_x].length) {
                cardinalFlag = planeFlags[region_x][region_y];
                if (CollisionUtils.noSouthernBoundary(baseFlag, cardinalFlag)) {
                    successors.add(cardinal);
                }
            }
            cardinal = current.derive(1, 0);
            region_x = cardinal.getX() - region_base_x;
            region_y = cardinal.getY() - region_base_y;
            if (region_x >= 0 && region_x < planeFlags.length && region_y >= 0 && region_y < planeFlags[region_x].length) {
                cardinalFlag = planeFlags[region_x][region_y];
                if (CollisionUtils.noEasternBoundary(baseFlag, cardinalFlag)) {
                    successors.add(cardinal);
                }
            }
            cardinal = current.derive(-1, 0);
            region_x = cardinal.getX() - region_base_x;
            region_y = cardinal.getY() - region_base_y;
            if (region_x >= 0 && region_x < planeFlags.length && region_y >= 0 && region_y < planeFlags[region_x].length) {
                cardinalFlag = planeFlags[region_x][region_y];
                if (CollisionUtils.noWesternBoundary(baseFlag, cardinalFlag)) {
                    successors.add(cardinal);
                }
            }
            return successors;
        }

        //TODO Convert to use a priority queue to vastly reduce the amount of repeated Distance calculations
        private static Coordinate popBasedOnDistance(
            final Coordinate source,
            final List<Coordinate> queue,
            final HashMap<Coordinate, Double> real_distances
        ) {
            Coordinate nearest = null;
            double ndistance = Double.POSITIVE_INFINITY;
            int nindex = -1;
            final Iterator<Coordinate> iterator = queue.iterator();
            for (int index = 0; iterator.hasNext(); ++index) {
                final Coordinate next = iterator.next();
                final double cdistance = real_distances.computeIfAbsent(next, coordinate -> Distance.between(source, next));
                if (nearest == null || cdistance < ndistance) {
                    nearest = next;
                    ndistance = cdistance;
                    nindex = index;
                }
            }
            queue.remove(nindex);
            return nearest;
        }
    }
}

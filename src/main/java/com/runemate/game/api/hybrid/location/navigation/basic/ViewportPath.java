package com.runemate.game.api.hybrid.location.navigation.basic;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.location.navigation.cognizant.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import javax.annotation.Nullable;
import lombok.*;
import org.jetbrains.annotations.*;

/**
 * Wraps around a CoordinatePath such as a {@link RegionPath} or a {@link BresenhamPath} and attempts to walk it via the viewport
 *
 * @deprecated Use Path.TraversalOption#PREFER_VIEWPORT instead
 */
@Deprecated
public final class ViewportPath extends Path {
    private final CoordinatePath wrapped;

    private ViewportPath(final CoordinatePath path) {
        this.wrapped = path;
    }

    /**
     * Converts the provided CoordinatePath into a ViewportPath
     *
     * @param path a preferably non-null CoordinatePath
     * @return A ViewportPath as long as path is not null
     */
    @Nullable
    public static ViewportPath convert(final CoordinatePath path) {
        if (path == null) {
            return null;
        }
        return new ViewportPath(path);
    }

    @Override
    public boolean step(@NonNull TraversalOption... options) {
        boolean toggleRunBeforeStepping =
            PlayerSense.getAsBoolean(PlayerSense.Key.TOGGLE_RUN_BEFORE_TRAVERSING);
        if (toggleRunBeforeStepping && !triggerRun(options)) {
            return false;
        }
        if (isEligibleToStep(options)) {
            Coordinate next = getNext();
            if (next != null) {
                if (!next.interact("Walk here")) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return triggerStaminaEnhancement(options) &&
            (toggleRunBeforeStepping || triggerRun(options));
    }

    @Override
    public List<Coordinate> getVertices() {
        return wrapped.getVertices();
    }

    @Override
    @Nullable
    public Coordinate getNext() {
        List<Coordinate> wrapped_vertices = getVertices();
        if (!wrapped_vertices.isEmpty()) {
            final List<Coordinate> reverse_sorted_vertices = new ArrayList<>(wrapped_vertices);
            Collections.reverse(reverse_sorted_vertices);
            //For performance reasons we store these two
            Shape viewport = Projection.getViewport();
            Coordinate region_base = Region.getBase();
            //Iterate with the furthest vertex so we find the furthest one on screen
            //so we can move the farthest along the path with each click.
            for (final Coordinate position : reverse_sorted_vertices) {
                if (position.isLoaded(region_base) && position.getVisibility(viewport) >= 95) {
                    return position;
                }
            }
        }
        return null;
    }

    @Override
    public Locatable getNext(boolean preferViewportTraversal) {
        return getNext();
    }
}
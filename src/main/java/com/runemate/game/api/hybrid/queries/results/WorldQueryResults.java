package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.local.*;
import java.util.*;
import java.util.concurrent.*;

public class WorldQueryResults extends QueryResults<WorldOverview, WorldQueryResults> {
    public WorldQueryResults(final Collection<? extends WorldOverview> results) {
        super(results);
    }

    public WorldQueryResults(
        final Collection<? extends WorldOverview> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    @Override
    protected WorldQueryResults get() {
        return this;
    }

    public WorldQueryResults sortById(boolean descending) {
        Comparator<WorldOverview> comparator = Comparator.comparingDouble(WorldOverview::getId);
        if (!descending) {
            comparator = comparator.reversed();
        }
        return sort(comparator);
    }
}

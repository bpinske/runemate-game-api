package com.runemate.game.api.hybrid.location.navigation.web;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.npcs.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;
import javafx.scene.canvas.*;
import lombok.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;

/**
 * A graph of nodes and vertices used to build WebPaths for complex navigation.
 * Webs are fast, can operate on regions that are not currently loaded, and can navigate with obstacles and teleports.
 */
@Log4j2
public class Web implements Renderable, IWeb {
    private final List<TeleportVertex> teleports;
    private final Map<Integer, List<WebVertex>> vertex_map;
    private final List<WebVertex> vertices;

    public Web() {
        this(0x8000);
    }

    public Web(int estimatedVertexCount) {
        this.vertex_map = new ConcurrentHashMap<>(estimatedVertexCount);
        this.vertices = Collections.synchronizedList(new ArrayList<>(estimatedVertexCount));
        this.teleports = Collections.synchronizedList(new ArrayList<>(estimatedVertexCount));
    }

    public Web(Collection<WebVertex> vertices) {
        this(vertices.size());
        addVertices(vertices);
    }

    public void addVertex(WebVertex vertex) {
        addVertex(vertex, true);
    }

    private void addVertex(WebVertex vertex, boolean addToList) {
        //Don't add it to the list if it was already added in a bulk addAll operation (such as within addVertices)
        if (addToList) {
            this.vertices.add(vertex);
        }
        List<WebVertex> existing = getVerticesOn(vertex.getPosition());
        if (existing != null) {
            if (vertex instanceof CoordinateVertex &&
                existing.stream().anyMatch(v -> v instanceof CoordinateVertex)) {
                throw new IllegalStateException(
                    "You cannot insert " + vertex + " into this Web as it is already occupied by " +
                        existing.stream().filter(v -> v instanceof CoordinateVertex).findAny()
                            .orElse(null));
            } else if (vertex instanceof SpiritTreeVertex) {
            } else if ((vertex instanceof ObjectVertex || vertex instanceof BankVertex) &&
                existing.stream().anyMatch(
                    v -> v instanceof ObjectVertex && !v.mustFulfillAnyOfRequirements.isEmpty())) {
                throw new IllegalStateException(
                    "You cannot insert " + vertex + " into this Web as it is already occupied by " +
                        existing.stream().filter(v -> v instanceof ObjectVertex &&
                            !v.mustFulfillAnyOfRequirements.isEmpty()).findAny().orElse(null));
            }
            existing.add(vertex);
        } else {
            this.vertex_map.put(
                vertex.getPosition().hashCode(),
                new ArrayList<>(Collections.singletonList(vertex))
            );
        }
        if (vertex instanceof TeleportVertex) {
            this.teleports.add((TeleportVertex) vertex);
        }
    }

    /**
     * Adds the vertices to the appropriate lists (i.e. basic, teleports, support, etc)
     */
    public final void addVertices(WebVertex... vertices) {
        addVertices(Arrays.asList(vertices));
    }

    public void addVertices(Collection<WebVertex> vertices) {
        this.vertices.addAll(vertices);
        for (final WebVertex vertex : vertices) {
            addVertex(vertex, false);
        }
    }

    public WebPathBuilder getPathBuilder() {
        return WebPathBuilder.create(this);
    }

    public Collection<TeleportVertex> getTeleports() {
        return teleports;
    }

    public WebVertex getVertexNearestTo(final Locatable locatable) {
        return getVertexNearestTo(locatable, WebVertex.class, false);
    }

    public WebVertex getVertexNearestTo(final Locatable locatable, boolean ignorePlane) {
        return getVertexNearestTo(locatable, WebVertex.class, ignorePlane);
    }

    public <T extends WebVertex> T getVertexNearestTo(
        final Locatable locatable,
        final Class<T> type
    ) {
        return getVertexNearestTo(locatable, type, false);
    }

    @SuppressWarnings("unchecked")
    public <T extends WebVertex> T getVertexNearestTo(
        final Locatable locatable,
        final Class<T> type, boolean ignorePlane
    ) {
        if (ignorePlane) {
            return (T) getVertexNearestTo(locatable, v -> type.isAssignableFrom(v.getClass()));
        }
        int plane = locatable.getPosition().getPlane();
        return (T) getVertexNearestTo(
            locatable,
            v -> v.getPosition().getPlane() == plane && type.isAssignableFrom(v.getClass())
        );
    }

    public WebVertex getVertexNearestTo(
        final Locatable locatable,
        final Predicate<WebVertex> predicate
    ) {
        if (vertices.isEmpty()) {
            return null;
        }
        Area area = locatable.getArea();
        if (area == null) {
            return null;
        }
        //Check if there are any vertices that match the predicate overlapping the Locatable.
        Coordinate center =
            locatable instanceof Coordinate ? (Coordinate) locatable : area.getCenter();
        List<WebVertex> overlapping = getVerticesWithin(area, predicate);
        if (overlapping != null && !overlapping.isEmpty()) {
            return overlapping.size() == 1 ? overlapping.get(0) :
                Sort.byDistanceFrom(center, overlapping, Distance.Algorithm.MANHATTAN).get(0);
        }
        //There aren't any vertices overlapping the Locatable that match the predicate, so use technique #2

        /*
         * http://stackoverflow.com/a/33639875
         * Searches in a counter-clockwise spiral starting with the coordinate to the right of the center.
         * Because we start to the right and break when steps >= 12, a maximum of 131 coordinates are searched.
         */
        WebVertex vertex;
        int x_off = 1, y_off = 0, direction = 1, steps = 1;
        while (steps < 12) {
            while (2 * x_off * direction < steps) {
                if ((vertex = getFirstVertexOn(center.derive(x_off, y_off), predicate)) != null) {
                    return vertex;
                }
                x_off += direction;
            }
            while (2 * y_off * direction < steps) {
                if ((vertex = getFirstVertexOn(center.derive(x_off, y_off), predicate)) != null) {
                    return vertex;
                }
                y_off += direction;
            }
            direction *= -1;
            steps++;
        }
        //Final desperate approach.
        double ndistance = Double.POSITIVE_INFINITY;
        Coordinate nearest = null;
        WebVertex gnearest = null;
        for (WebVertex gcurrent : vertices) {
            Coordinate current = gcurrent.getPosition();
            if (!predicate.test(gcurrent)) {
                continue;
            }
            double cdistance =
                Distance.Algorithm.MANHATTAN.calculate(center.getX(), center.getY(), current.getX(),
                    current.getY()
                );// + (Math.max(nearest.getPlane(), current.getPlane()) - Math.min(nearest.getPlane(), current.getPlane()) * 5);
            if (nearest == null || cdistance < ndistance) {
                nearest = current;
                gnearest = gcurrent;
                ndistance = cdistance;
            }
        }
        return gnearest;
    }

    /**
     * See Web#getVerticesOn or Web#getFirstVertexOn
     *
     * @param coordinate
     * @return
     */
    @Deprecated
    public WebVertex getVertexOn(Coordinate coordinate) {
        return getFirstVertexOn(coordinate);
    }

    public WebVertex getFirstVertexOn(Coordinate coordinate) {
        return getFirstVertexOn(coordinate, (Predicate<WebVertex>) null);
    }

    @SuppressWarnings("unchecked")
    public <T extends WebVertex> T getFirstVertexOn(Coordinate coordinate, Class<T> type) {
        return (T) getVertexOn(coordinate, v -> type.isAssignableFrom(v.getClass()));
    }

    public WebVertex getFirstVertexOn(Coordinate coordinate, Predicate<WebVertex> predicate) {
        List<WebVertex> vertices = getVerticesOn(coordinate);
        if (vertices != null) {
            if (vertices.isEmpty()) {
                throw new IllegalStateException(
                    "A coordinate should not have an empty set of vertices.");
            }
            for (WebVertex vertex : vertices) {
                if (vertex != null && (predicate == null || predicate.test(vertex))) {
                    return vertex;
                }
            }
        }
        return null;
    }

    @Nullable
    public List<WebVertex> getVerticesOn(@NonNull Coordinate coordinate) {
        return vertex_map.get(coordinate.hashCode());
    }

    public boolean hasVerticesOn(@NonNull Coordinate coordinate) {
        return vertex_map.containsKey(coordinate.hashCode()) &&
            vertex_map.get(coordinate.hashCode()).size() > 0;
    }

    public long countVerticesOn(@NonNull Coordinate coordinate) {
        return vertex_map.getOrDefault(coordinate.hashCode(), Collections.emptyList()).size();
    }

    @NonNull
    public List<WebVertex> getVerticesWithin(@NonNull Area area) {
        List<Coordinate> tiles = area.getCoordinates();
        List<WebVertex> vertices = new ArrayList<>(tiles.size());
        tiles.stream().map(this::getVerticesOn).filter(Objects::nonNull).forEach(vertices::addAll);
        return vertices;
    }

    @NonNull
    public <T extends WebVertex> List<T> getVerticesWithin(
        @NonNull Area area,
        final Class<T> type
    ) {
        ArrayList<T> vertices = new ArrayList<>();
        for (Coordinate coordinate : area.getCoordinates()) {
            List<T> on = getVerticesOn(coordinate, type);
            if (on != null) {
                vertices.addAll(on);
            }
        }
        return vertices;
    }

    @NonNull
    public List<WebVertex> getVerticesWithin(
        @NonNull Area area,
        final Predicate<WebVertex> predicate
    ) {
        ArrayList<WebVertex> vertices = new ArrayList<>();
        for (Coordinate coordinate : area.getCoordinates()) {
            List<WebVertex> on = getVerticesOn(coordinate, predicate);
            if (on != null) {
                vertices.addAll(on);
            }
        }
        return vertices;
    }

    public boolean hasVerticesWithin(@NonNull Area area) {
        return area.getCoordinates().stream().map(this::getVerticesOn)
            .anyMatch(on -> on != null && on.size() > 0);
    }

    public long countVerticesWithin(@NonNull Area area) {
        return area.getCoordinates().stream().map(this::getVerticesOn)
            .filter(on -> on != null && on.size() > 0).count();
    }

    /**
     * See Web#getVerticesOn or Web#getFirstVertexOn
     *
     * @param coordinate
     * @return
     */
    @Deprecated
    public final WebVertex getVertexOn(Coordinate coordinate, Predicate<WebVertex> predicate) {
        return getFirstVertexOn(coordinate, predicate);
    }

    /**
     * See Web#getVerticesOn or Web#getFirstVertexOn
     *
     * @param coordinate
     * @return
     */
    @Deprecated
    public final <T extends WebVertex> T getVertexOn(Coordinate coordinate, Class<T> type) {
        return getFirstVertexOn(coordinate, type);
    }

    public final <T extends WebVertex> List<T> getVerticesOn(Coordinate coordinate, Class<T> type) {
        List<WebVertex> vertices = getVerticesOn(coordinate);
        return vertices == null ? null :
            vertices.stream().filter(v -> type.isAssignableFrom(v.getClass())).map(type::cast)
                .collect(Collectors.toList());
    }

    public final List<WebVertex> getVerticesOn(Coordinate coordinate, Predicate<WebVertex> filter) {
        List<WebVertex> vertices = getVerticesOn(coordinate);
        return vertices != null ? vertices.stream().filter(filter).collect(Collectors.toList()) :
            null;
    }

    public final <T extends WebVertex> List<T> getVertices(Class<T> clazz) {
        return getVertices().stream().filter(wv -> clazz.isAssignableFrom(wv.getClass()))
            .map(clazz::cast).collect(Collectors.toList());
    }

    public List<WebVertex> getVertices() {
        return vertices;
    }

    /**
     * Uses BFS to calculate the reachable vertices from the given root
     */
    public Collection<WebVertex> getVerticesReachableFrom(final WebVertex source) {
        //Hashset has O(1) contains
        final HashSet<WebVertex> visited = new HashSet<>();
        final LinkedHashSet<WebVertex> queue = new LinkedHashSet<>();
        queue.add(source);
        while (!queue.isEmpty()) {
            final WebVertex vertex = queue.iterator().next();
            visited.add(vertex);
            for (final WebVertex successor : vertex.getOutputs()) {
                if (!visited.contains(successor)) {
                    queue.add(successor);
                }
            }
            queue.remove(vertex);
        }
        return visited;
    }

    public final void removeVertex(WebVertex vertex) {
        this.vertices.remove(vertex);
        List<WebVertex> vertices = this.vertex_map.get(vertex.getPosition().hashCode());
        if (vertices != null) {
            vertices.remove(vertex);
            if (vertices.isEmpty()) {
                this.vertex_map.remove(vertex.getPosition().hashCode());
            }
        }
        if (vertex instanceof TeleportVertex) {
            this.teleports.remove(vertex);
        }
    }

    /**
     * Removes the vertices from the appropriate lists (i.e. basic, bank, or teleports)
     */
    public final void removeVertices(WebVertex... vertices) {
        removeVertices(Arrays.asList(vertices));
    }

    public final int removeVertices(Predicate<WebVertex> removal_condition) {
        Set<WebVertex> removable = Parallelize.collect(vertices, removal_condition);
        removeVertices(removable);
        return removable.size();
    }

    public void removeVertices(Collection<WebVertex> vertices) {
        HashSet<? extends WebVertex> set =
            vertices instanceof HashSet ? (HashSet<? extends WebVertex>) vertices :
                new HashSet<>(vertices);
        List<Map.Entry<Integer, List<WebVertex>>> verticesList =
            new ArrayList<>(this.vertex_map.entrySet());
        for (Map.Entry<Integer, List<WebVertex>> vertex_map_entry : verticesList) {
            vertex_map_entry.getValue().removeAll(set);
            if (vertex_map_entry.getValue().isEmpty()) {
                this.vertex_map.remove(vertex_map_entry.getKey());
            }
        }
        this.vertices.removeAll(set);
        this.teleports.removeAll(set);
    }

    public void retainVertices(Collection<WebVertex> vertices) {
        try {
            Set<? extends WebVertex> set =
                vertices instanceof Set ? (Set<? extends WebVertex>) vertices :
                    new HashSet<>(vertices);
            List<Map.Entry<Integer, List<WebVertex>>> verticesList =
                new ArrayList<>(this.vertex_map.entrySet());
            for (Map.Entry<Integer, List<WebVertex>> vertex_map_entry : verticesList) {
                vertex_map_entry.getValue().retainAll(set);
                if (vertex_map_entry.getValue().isEmpty()) {
                    this.vertex_map.remove(vertex_map_entry.getKey());
                }
            }
            this.vertices.retainAll(set);
            this.teleports.retainAll(set);
        } catch (Throwable t) {
            log.warn(t);
        }
    }

    /**
     * See replaceVertices
     *
     * @param vertex
     * @return
     */
    @Deprecated
    public final WebVertex replaceVertex(WebVertex vertex) {
        return replaceVertex(vertex, null);
    }

    /**
     * See replaceVertices
     *
     * @param vertex
     * @return
     */
    @Deprecated
    public final WebVertex replaceVertex(WebVertex vertex, double cost) {
        return replaceVertex(vertex, cost > 0 ? ((v1, v2) -> cost) : null);
    }

    /**
     * See replaceVertices
     * TODO document and improve
     *
     * @param vertex
     * @param weight_calculator
     * @return
     */
    @Deprecated
    public final WebVertex replaceVertex(
        WebVertex vertex,
        BiFunction<WebVertex, WebVertex, Double> weight_calculator
    ) {
        List<WebVertex> vertices = replaceVertices(vertex, weight_calculator);
        return vertices == null || vertices.isEmpty() ? null : vertices.get(0);
    }

    public final List<WebVertex> replaceVertices(
        WebVertex vertex,
        BiFunction<WebVertex, WebVertex, Double> weight_calculator
    ) {
        Coordinate position = vertex.getPosition();
        List<WebVertex> replacing = getVerticesOn(position);
        if (replacing == null) {
            throw new IllegalStateException(
                "You cannot replace a vertex at " + position + " if it's already unoccupied.");
        }
        if (!vertex.isIsolated()) {
            throw new IllegalStateException(
                "You cannot use a vertex that has an edge as a replacement.");
        }
        for (WebVertex v : replacing) {
            for (Map.Entry<WebVertex, Double> output : v.getOutputCosts().entrySet()) {
                v.removeDirectedEdge(output.getKey());
                vertex.addDirectedEdge(
                    output.getKey(),
                    weight_calculator == null ? output.getValue() :
                        weight_calculator.apply(vertex, output.getKey())
                );
            }
        }
        for (WebVertex v : replacing) {
            for (Map.Entry<WebVertex, Double> input : v.getInputCosts().entrySet()) {
                input.getKey().removeDirectedEdge(v);
                input.getKey().addDirectedEdge(
                    vertex,
                    weight_calculator == null ? input.getValue() :
                        weight_calculator.apply(input.getKey(), vertex)
                );
            }
        }
        this.vertices.removeAll(replacing);
        this.vertices.add(vertex);
        //Collections.replaceAll(this.vertices, replacing, vertex);
        this.vertex_map.put(
            position.hashCode(),
            new ArrayList<>(Collections.singletonList(vertex))
        );
        //Find a faster way to replace without the necessary cast checks?
        if (replacing instanceof TeleportVertex) {
            this.teleports.remove(replacing);
        }
        if (vertex instanceof TeleportVertex) {
            this.teleports.add((TeleportVertex) vertex);
        }
        return replacing;
    }

    @Override
    public void render(Graphics2D g2d) {
        Player local = Players.getLocal();
        if (local == null) {
            return;
        }
        Coordinate localPosition = local.getPosition();
        Shape viewport = /*Projection.getViewport();*/null;
        Coordinate base = Region.getBase();
        Map<Coordinate, Polygon> reusable = new HashMap<>();
        Map<String, Object> renderPerformanceCache = new HashMap<>();
        for (WebVertex vertex : getVerticesWithin(
            Area.rectangular(localPosition.derive(-5, -5), localPosition.derive(5, 5)))) {
            if (!vertex.isUsable()) {
                continue;
            }
            Coordinate vertexCoordinate = vertex.getPosition();
            Polygon vertexTilePolygon = reusable.computeIfAbsent(
                vertexCoordinate,
                vc -> vc.getBounds(viewport, base, renderPerformanceCache)
            );
            if (vertexTilePolygon == null) {
                continue;
            }
            if (vertex instanceof NpcVertex) {
                g2d.setColor(new Color(0, 255, 0, 102));
            } else if (vertex instanceof TeleportVertex) {
                g2d.setColor(new Color(255, 255, 0, 153));
            } else if (vertex instanceof UtilityVertex) {
                g2d.setColor(new Color(255, 155, 160, 102));
            }
            InteractablePolygon vertexShape = new InteractablePolygon(vertexTilePolygon);
            if (vertex instanceof NpcVertex || vertex instanceof TeleportVertex ||
                vertex instanceof UtilityVertex) {
                //Draw the semi-transparent fill
                vertexShape.render(g2d, true);
            }
            //Now add the outline/stroke
            if (vertex instanceof CoordinateVertex) {
                g2d.setColor(Color.RED);
            } else if (vertex instanceof ObjectVertex) {
                g2d.setColor(Color.BLUE);
            } else if (vertex instanceof NpcVertex) {
                g2d.setColor(Color.GREEN);
            } else if (vertex instanceof TeleportVertex) {
                g2d.setColor(Color.YELLOW);
            } else if (vertex instanceof UtilityVertex) {
                g2d.setColor(new Color(255, 155, 160));
            }
            vertexShape.render(g2d, false);
            g2d.setColor(Color.MAGENTA);
            Rectangle2D vertexBounds = vertexShape.getBounds2D();
            for (WebVertex output : vertex.getOutputs()) {
                Coordinate outputCoordinate = output.getPosition();
                Polygon outputVertexTilePolygon = reusable.computeIfAbsent(
                    outputCoordinate,
                    oc -> oc.getBounds(viewport, base, renderPerformanceCache)
                );
                if (outputVertexTilePolygon == null) {
                    continue;
                }
                Rectangle2D bounds2D = outputVertexTilePolygon.getBounds2D();
                g2d.drawLine((int) vertexBounds.getCenterX(), (int) vertexBounds.getCenterY(),
                    (int) bounds2D.getCenterX(), (int) bounds2D.getCenterY()
                );
                g2d.fillOval((int) bounds2D.getCenterX() - 2, (int) bounds2D.getCenterY() - 2, 5,
                    5
                );
            }
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        Player local = Players.getLocal();
        if (local == null) {
            return;
        }
        Coordinate localPosition = local.getPosition();
        Shape viewport = /*Projection.getViewport();*/null;
        Coordinate base = Region.getBase();
        Map<Coordinate, Polygon> reusable = new HashMap<>();
        Map<String, Object> renderPerformanceCache = new HashMap<>();
        for (WebVertex vertex : getVerticesWithin(
            Area.rectangular(localPosition.derive(-5, -5), localPosition.derive(5, 5)))) {
            if (!vertex.isUsable()) {
                continue;
            }
            Coordinate vertexCoordinate = vertex.getPosition();
            Polygon vertexTilePolygon = reusable.computeIfAbsent(
                vertexCoordinate,
                vc -> vc.getBounds(viewport, base, renderPerformanceCache)
            );
            if (vertexTilePolygon == null) {
                continue;
            }
            if (vertex instanceof CoordinateVertex) {
                gc.setStroke(javafx.scene.paint.Color.RED);
            } else if (vertex instanceof ObjectVertex) {
                gc.setStroke(javafx.scene.paint.Color.BLUE);
            } else if (vertex instanceof NpcVertex) {
                gc.setFill(javafx.scene.paint.Color.GREEN.deriveColor(0, 0, 0, 0.4));
                gc.setStroke(javafx.scene.paint.Color.DARKGRAY);
            } else if (vertex instanceof TeleportVertex) {
                gc.setFill(javafx.scene.paint.Color.YELLOW.deriveColor(0, 0, 0, 0.6));
                gc.setStroke(javafx.scene.paint.Color.DARKGRAY);
            } else if (vertex instanceof UtilityVertex) {
                gc.setFill(javafx.scene.paint.Color.ORANGE.deriveColor(0, 0, 0, 0.4));
                gc.setStroke(javafx.scene.paint.Color.DARKGRAY);
            }
            InteractablePolygon vertexShape = new InteractablePolygon(vertexTilePolygon);
            if (vertex instanceof NpcVertex || vertex instanceof TeleportVertex ||
                vertex instanceof UtilityVertex) {
                vertexShape.render(gc, true);
            }
            vertexShape.render(gc, false);
            gc.setStroke(javafx.scene.paint.Color.PURPLE);
            Rectangle2D vertexBounds = vertexShape.getBounds2D();
            for (WebVertex output : vertex.getOutputs()) {
                Coordinate outputCoordinate = output.getPosition();
                Polygon outputVertexTilePolygon = reusable.computeIfAbsent(
                    outputCoordinate,
                    oc -> oc.getBounds(viewport, base, renderPerformanceCache)
                );
                if (outputVertexTilePolygon == null) {
                    continue;
                }
                Rectangle2D bounds2D = outputVertexTilePolygon.getBounds2D();
                gc.strokeLine(vertexBounds.getCenterX(), vertexBounds.getCenterY(),
                    bounds2D.getCenterX(), bounds2D.getCenterY()
                );
                gc.fillOval(bounds2D.getCenterX() - 2.0, bounds2D.getCenterY() - 2.0, 5.0, 5.0);
            }
        }
    }
}

package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.internal.exception.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import org.jetbrains.annotations.*;

public class EnumDefinitions {
    private static final EnumDefinitionLoader LOADER = new EnumDefinitionLoader(CacheIndex.CONFIGS.getId());

    @Nullable
    public static EnumDefinition load(int id) {
        try {
            EnumDefinition definition = LOADER.load(ConfigType.ENUM, id);
            if (definition != null && definition.getEntryCount() > 0) {
                return definition;
            }
        } catch (IOException e) {
            throw new UnableToParseBufferException(
                "Failed to load EnumDefinition from the Js5Cache.", e);
        }
        return null;
    }

    /**
     * Loads all definitions
     */
    public static List<EnumDefinition> loadAll() {
        return loadAll(null);
    }

    /**
     * Loads all definitions that are accepted by the filter
     */
    public static List<EnumDefinition> loadAll(final Predicate<EnumDefinition> filter) {
        int quantity = LOADER.getFiles(ConfigType.ENUM).length;
        ArrayList<EnumDefinition> definitions = new ArrayList<>(quantity);
        for (int id = 0; id <= quantity; ++id) {
            final EnumDefinition definition = load(id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }
}

package com.runemate.game.api.hybrid.input;

import com.runemate.game.api.hybrid.entities.details.*;

public class HoppingPathGenerator extends Mouse.PathGenerator {
    @Override
    public boolean move(Interactable target, double ignored) {
        return target != null && Mouse.PathGenerator.hop(target.getInteractionPoint());
    }
}
